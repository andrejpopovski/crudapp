package mk.andrej.crupApp.api;

import java.util.Map;

import mk.andrej.crudApp.domain.Student;
import mk.andrej.crudApp.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("student")
public class StudentController {

	@Autowired
	StudentService studentService;

	@GetMapping
	public Map<Integer, Student> readAll() {
		return studentService.getAll();
	}

	@GetMapping("/{id}")
	public void readId(@PathVariable(value = "id") Integer id) {
		studentService.findById(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable(value = "id") Integer id) {
		studentService.delete(id);

	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@PathVariable(value = "id") Integer id, @RequestBody Student student) {
		studentService.update(id, student);
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public void add(@RequestBody Student student) {
		studentService.create(student);
	}

}
