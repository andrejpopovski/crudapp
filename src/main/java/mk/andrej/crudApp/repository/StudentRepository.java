package mk.andrej.crudApp.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mk.andrej.crudApp.domain.Student;

@Repository
public interface StudentRepository extends CrudRepository<Integer, Student> {

	public Optional<Student> findById(Integer id);

	public void delete(Student student);

	public void save(Student student);

}
