package mk.andrej.crudApp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Student {

	private int id;
	@NonNull
	private String firstName;
	@NonNull
	private String lastName;

}
