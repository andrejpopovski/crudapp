package mk.andrej.crudApp.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.andrej.crudApp.domain.Student;
import mk.andrej.crudApp.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Student findById(Integer id) {
		Student student = studentRepository.findById(id).orElseThrow();
		return student;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Student> getAll() {
		return (Map<Integer, Student>) studentRepository.findAll();
	}

	@Override
	public void delete(Integer id) {
		Student student = findById(id);
		studentRepository.delete(student);
	}

	@Override
	public Student create(Student student) {

		studentRepository.save(student);

		return student;
	}

	@Override
	public Student update(Integer id, Student student) {
		Student student1 = findById(id);

		studentRepository.save(student);

		return student1;
	}

}
