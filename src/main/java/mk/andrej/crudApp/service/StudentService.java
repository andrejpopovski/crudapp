package mk.andrej.crudApp.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import mk.andrej.crudApp.domain.Student;

@Service
public interface StudentService {

	public Student findById(Integer id);

	public Map<Integer, Student> getAll();

	public Student create(Student student);

	public Student update(Integer id, Student student);

	public void delete(Integer id);

}
